import cv2
import csv
import numpy as np

from skimage import metrics
from operator import itemgetter
from os import listdir, remove
from os.path import isfile, join
from shutil import copyfile
from sys import exit
from sys import exc_info

# constantes
MIN_VALUE = 0.90
EXT = ".jpg"

def createGabarito(image, tipo):
    cv2.namedWindow('original', cv2.WINDOW_NORMAL)
    cv2.resizeWindow('original', 600, 600)
    cv2.imshow('original', image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    gray =  cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    th = cv2.threshold(gray, 70, 255, cv2.THRESH_BINARY)[1] ## apenas segundo valor de return, ou seja, thresh

    cv2.namedWindow('binarização', cv2.WINDOW_NORMAL)
    cv2.resizeWindow('binarização', 600, 600)
    cv2.imshow('binarização', th)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    kernel = np.ones((5,5), np.uint8)

    # opened = cv2.morphologyEx(th, cv2.MORPH_OPEN, kernel)
    closed = cv2.morphologyEx(th, cv2.MORPH_CLOSE, kernel)

    # cv2.namedWindow('abertura', cv2.WINDOW_NORMAL)
    # cv2.resizeWindow('abertura', 600, 600)
    # cv2.imshow('abertura', opened)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()

    cv2.namedWindow('fechamento', cv2.WINDOW_NORMAL)
    cv2.resizeWindow('fechamento', 600, 600)
    cv2.imshow('fechamento', closed)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    e = cv2.Canny(closed, 0, 255)

    cv2.namedWindow('bordas', cv2.WINDOW_NORMAL)
    cv2.resizeWindow('bordas', 600, 600)
    cv2.imshow('bordas', e)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    contours, hierarchy = cv2.findContours(e, 
        cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    
    # print("Foram encontrados", str(len(contours)), "contornos.")
    
    contours.sort(key = lambda x: cv2.contourArea(x), reverse = True)

    a,b,xx,yy = cv2.boundingRect(contours[0]) ## h, w
    x,y,c,d = cv2.boundingRect(contours[1]) ## x0, y0

    # print("x:", x, "y:", y)
    # print("h:", xx, "w:", yy)

    y0 = y
    x += c

    print("x inicial:", x)
    print("y inicial:", y)
    print("largura:", xx)
    print("altura:", yy)
    
    # dicionario da folha de respostas
    fr = {}

    # resize gabarito
    g = cv2.imread("images/gabarito/g0.png", 0)
    gA = cv2.resize(g, (xx, yy), interpolation=cv2.INTER_AREA)

    g = cv2.imread("images/gabarito/g1.png", 0)
    gB = cv2.resize(g, (xx, yy), interpolation=cv2.INTER_AREA)

    g = cv2.imread("images/gabarito/g2.png", 0)
    gC = cv2.resize(g, (xx, yy), interpolation=cv2.INTER_AREA)

    g = cv2.imread("images/gabarito/g3.png", 0)
    gD = cv2.resize(g, (xx, yy), interpolation=cv2.INTER_AREA)

    g = cv2.imread("images/gabarito/g4.png", 0)
    gE = cv2.resize(g, (xx, yy), interpolation=cv2.INTER_AREA)

    # dicionario de questões
    questions = {"a": 0, "b": 0, "c": 0, "d": 0, "e": 0}

    for i in range(50):
        q = image[y:(y+yy), x:(x+xx)]

        kernel = np.ones((9,9), np.uint8)
        g = cv2.cvtColor(q, cv2.COLOR_BGR2GRAY)
        b = cv2.threshold(g,0,255,cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)[1]
        
        cv2.imshow("binarização", b)
        cv2.waitKey(0)
        
        opened = cv2.morphologyEx(b, cv2.MORPH_OPEN, kernel)
        closed = cv2.morphologyEx(opened, cv2.MORPH_CLOSE, kernel)
        
        cv2.imshow("abertura", closed)
        cv2.waitKey(0)

        cv2.imshow("fechamento", closed)
        cv2.waitKey(0)

        M = cv2.moments(closed)
        if M["m00"] != 0:
            cX = int(M["m10"] / M["m00"])
            cY = int(M["m01"] / M["m00"])
            cv2.circle(q, (cX, cY), 5, (255, 255, 255), -1)
        else:
            cX, cY = int(xx/2), int(yy/2) - 7*(i%2)

        print('centro da resposta', (cX, cY))
        
        if(cY > int(yy/2)):
            print("cY > meio. y =", y)
            y = y + yy + (cY - int(yy/2))
            print("y ajustado =", y)
        elif(cY <= int(yy/2)):
            print("cY < meio: y =", y)
            y = y + yy - (int(yy/2) - cY)
            print("y ajustado =", y)
        else:
            y = y + yy

        if(i == 19 or i == 39):
                x = x+xx
                y = y0

        # verifica a alternativa correta
        questions["a"] = metrics.structural_similarity(gA, closed)
        questions["b"] = metrics.structural_similarity(gB, closed)
        questions["c"] = metrics.structural_similarity(gC, closed)
        questions["d"] = metrics.structural_similarity(gD, closed)
        questions["e"] = metrics.structural_similarity(gE, closed)
        print("score da questão ", str(i+1))
        for k, v in questions.items():
            print("letra", k, str(": {:.3f}".format(v)))

        answer = max(questions.items(), key=itemgetter(1))
        if(answer[1] >= MIN_VALUE and M["m00"] != 0):
            fr[i+1] = answer[0]
            print("alternativa correta é a letra", answer[0], "com {:.3f}%".format(answer[1]*100))
            print("valor salvo:", fr[i+1])
        else:
            fr[i+1] = 'x'
            print("alternativa mais próxima da correta é a letra", answer[0], "com {:.3f}%".format(answer[1]*100))
            print("valor salvo:", fr[i+1])
        
        cv2.imshow("questão", q)
        cv2.waitKey(0)
    cv2.destroyAllWindows()

    # print(fr)
    outputPath = "files/gabarito/gabarito-prova" + str(tipo+1) + ".csv"
    ans = open(outputPath, "w")
    w = csv.writer(ans)
    for key, val in fr.items():
        w.writerow([key, val])
    ans.close()

    try:
        f = open(outputPath)
        return True
    except IOError:
        print("Arquivo não criado")
        return False
    finally:
        f.close()

def createPeso(path, tipo):
    print(path)
    source = path[0]
    target = "files/peso/peso-prova" + str(tipo+1) + ".csv"

    try:
        copyfile(source, target)
    except IOError as e:
        print("Unable to copy file. %s" % e)
        exit(1)
    except:
        print("Unexpected error:", exc_info())
        exit(1)

    print("\nFile copy done!\n")
    return True

def findAnswers(path):
    onlyfiles = [f for f in listdir(path) if isfile(join(path, f))]
    for file in onlyfiles:
        
        filename = path + "/" + file
        print(filename)
        
        image = cv2.imread(filename)

        # cv2.namedWindow('original', cv2.WINDOW_NORMAL)
        # cv2.resizeWindow('original', 600, 600)
        # cv2.imshow('original', image)
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()

        gray =  cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        th = cv2.threshold(gray, 70, 255, cv2.THRESH_BINARY)[1] ## apenas segundo valor de return, ou seja, thresh

        # cv2.namedWindow('binarização', cv2.WINDOW_NORMAL)
        # cv2.resizeWindow('binarização', 600, 600)
        # cv2.imshow('binarização', th)
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()

        kernel = np.ones((5,5), np.uint8)

        opened = cv2.morphologyEx(th, cv2.MORPH_OPEN, kernel)
        closed = cv2.morphologyEx(opened, cv2.MORPH_CLOSE, kernel)

        # cv2.namedWindow('abertura', cv2.WINDOW_NORMAL)
        # cv2.resizeWindow('abertura', 600, 600)
        # cv2.imshow('abertura', opened)
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()

        # cv2.namedWindow('fechamento', cv2.WINDOW_NORMAL)
        # cv2.resizeWindow('fechamento', 600, 600)
        # cv2.imshow('fechamento', closed)
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()

        e = cv2.Canny(closed, 0, 255)

        # cv2.namedWindow('bordas', cv2.WINDOW_NORMAL)
        # cv2.resizeWindow('bordas', 600, 600)
        # cv2.imshow('bordas', e)
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()

        contours, hierarchy = cv2.findContours(e, 
            cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
        
        # print("Foram encontrados", str(len(contours)), "contornos.")
        
        contours.sort(key = lambda x: cv2.contourArea(x), reverse = True)

        a,b,xx,yy = cv2.boundingRect(contours[0]) ## h, w
        x,y,c,d = cv2.boundingRect(contours[1]) ## x0, y0

        # print("x:", x, "y:", y)
        # print("h:", xx, "w:", yy)

        y0 = y
        x += c

        # print("q", x, y, xx, yy)
        
        # dicionario da folha de respostas
        fr = {}

        # resize gabarito
        g = cv2.imread("images/gabarito/g0.png", 0)
        gA = cv2.resize(g, (xx, yy), interpolation=cv2.INTER_AREA)

        g = cv2.imread("images/gabarito/g1.png", 0)
        gB = cv2.resize(g, (xx, yy), interpolation=cv2.INTER_AREA)

        g = cv2.imread("images/gabarito/g2.png", 0)
        gC = cv2.resize(g, (xx, yy), interpolation=cv2.INTER_AREA)

        g = cv2.imread("images/gabarito/g3.png", 0)
        gD = cv2.resize(g, (xx, yy), interpolation=cv2.INTER_AREA)

        g = cv2.imread("images/gabarito/g4.png", 0)
        gE = cv2.resize(g, (xx, yy), interpolation=cv2.INTER_AREA)

        # dicionario de questões
        questions = {"a": 0, "b": 0, "c": 0, "d": 0, "e": 0}

        for i in range(50):
            q = image[y:(y+yy), x:(x+xx)]

            kernel = np.ones((9,9), np.uint8)
            g = cv2.cvtColor(q, cv2.COLOR_BGR2GRAY)
            b = cv2.threshold(g,127,255,cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)[1]

            # cv2.imshow("binarização", b)
            # cv2.waitKey(0)

            opened = cv2.morphologyEx(b, cv2.MORPH_OPEN, kernel)
            closed = cv2.morphologyEx(opened, cv2.MORPH_CLOSE, kernel)

            # cv2.imshow("abertura", closed)
            # cv2.waitKey(0)

            # cv2.imshow("fechamento", closed)
            # cv2.waitKey(0)

            M = cv2.moments(closed)
            if M["m00"] != 0:
                cX = int(M["m10"] / M["m00"])
                cY = int(M["m01"] / M["m00"])
            else:
                cX, cY = int(xx/2), int(yy/2) - 7*(i%2)

            # cv2.circle(q, (cX, cY), 5, (255, 255, 255), -1)
            # print('center', (cX, cY))
            
            if(cY > int(yy/2)):
                y = y + yy + (cY - int(yy/2))
                # print("cY >: cY =", cY, "Y =", y)
            elif(cY < int(yy/2)):
                y = y + yy - (int(yy/2) - cY)
                # print("cY <: cY =", cY, "Y =", y)
            else:
                y = y + yy

            if(i == 19 or i == 39):
                    x = x+xx
                    y = y0

            # verifica a alternativa correta
            questions["a"] = metrics.structural_similarity(gA, closed)
            questions["b"] = metrics.structural_similarity(gB, closed)
            questions["c"] = metrics.structural_similarity(gC, closed)
            questions["d"] = metrics.structural_similarity(gD, closed)
            questions["e"] = metrics.structural_similarity(gE, closed)
            print("score da questão ", str(i+1))
            for k, v in questions.items():
                print("letra", k, str(": {:.3f}".format(v)))

            answer = max(questions.items(), key=itemgetter(1))
            if(answer[1] >= MIN_VALUE and M["m00"] != 0):
                fr[i+1] = answer[0]
                print("alternativa correta é a letra", answer[0], "com {:.3f}%".format(answer[1]*100))
                print("valor salvo:", fr[i+1])
            else:
                fr[i+1] = 'x'
                print("alternativa mais próxima da correta é a letra", answer[0], "com {:.3f}%".format(answer[1]*100))
                print("valor salvo:", fr[i+1])

            # cv2.imshow("questão", q)
            # cv2.waitKey(0)
        cv2.destroyAllWindows()

        # print(fr)
        csvName = file.split('.')[0]
        outputPath = "files/provas/" + csvName + ".csv"
        ans = open(outputPath, "w")
        w = csv.writer(ans)
        for key, val in fr.items():
            w.writerow([key, val])
        ans.close()

        try:
            f = open(outputPath)
            print("Arquivo salvo com sucesso!")
        except IOError:
            print("Arquivo não criado")
            return False
        finally:
            f.close()
    return True

def showScore(tipo):
    peso = {}
    resp = {}
    fr = {}
    resultado = {}
    notas = {}
    # abre gabarito
    gabaritoName = "files/gabarito/gabarito-prova" + str(tipo+1) + ".csv"
    with open(gabaritoName) as gab:
        r = csv.reader(gab)
        for rows in r:
            resp[int(rows[0])] = rows[1]
    
    # abre peso
    pesoName = "files/peso/peso-prova" + str(tipo+1) + ".csv"
    with open(pesoName) as p:
        r = csv.reader(p)
        for rows in r:
            peso[int(rows[0])] = int(rows[1])
    
    # abre folhas de resposta
    frPath = "files/provas/"
    onlyfiles = [f for f in listdir(frPath) if isfile(join(frPath, f)) and f.endswith(".csv")]
    for file in onlyfiles:
        print(file)
        filename = frPath + "/" + file
        with open(filename) as prova:
            r = csv.reader(prova)
            for rows in r:
                fr[int(rows[0])] = rows[1]
        
        for i in range(1,51):
            if(fr[i] == resp[i]):
                resultado[i] = peso[i]
            else:
                resultado[i] = 0
        nota = 0
        for value in resultado:
            nota += resultado[value]
        nota /= 10
        name = file.split(".")[0]
        print("nota do ", name, "é", nota)
        notas[name] = nota
        
    ans = open("files/answers.csv", "w")
    w = csv.writer(ans)
    for key, value in notas.items():
        w.writerow([key, value])
    ans.close()
    try:
        ans = open("files/answers.csv")
        print("Arquivo salvo com sucesso!")
        return True
    except IOError:
        print("File not accessible")
        return False
    finally:
        ans.close()

