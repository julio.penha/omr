Para baixar o zip, clique aqui: https://gitlab.com/juliopenha/omr/-/archive/master/omr-master.zip

Atualizações: 22/05
# Para executar sem instalar
1. Abrir pasta Exec;
2. Abrir pasta OMR-Python-windows;
3. Executar programa OMR-Python;
4. Selecionar imagem de Gabarito da pasta de sua preferência ou da pasta Gabarito
5. Selecionar arquivo csv com o peso da prova de uma pasta de sua preferência ou da pasta Pesos;
6. Selecionar pasta contendo uma ou mais imagens de Folha de Respostas de uma pasta de preferência ou da pasta FR;
7. Clicar em Mostrar Resultados;
8. Os arquivos para conferência ("gabarito-provaX.csv", "peso-provaX.csv" e "prova_aula.csv") estão na pasta "files" dentro da pasta OMR-Python-windows;
9. Todos os arquivos são deletados quando o programa é iniciado e a cada nova execução.

# Bibliotecas Necessárias para execução:
- PyQt5: `pip install pyqt5-tools`
- OpenCV: `pip install opencv-python`
- SkImage: `pip install scikit-image`
