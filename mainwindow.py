# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'mainwindow.ui'
#
# Created by: PyQt5 UI code generator 5.15.0
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QFileDialog, QWidget, QTableWidget, QTableWidgetItem, QGridLayout

from findAnswers import findAnswers, createGabarito, createPeso, showScore

from os import listdir, remove
from os.path import isfile, join, exists

import cv2
import csv

prova = 0

class Ui_MainWindow(QWidget):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(534, 400)
        MainWindow.setMinimumSize(QtCore.QSize(400, 400))
        MainWindow.setMaximumSize(QtCore.QSize(1366, 768))
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("./images/anvil.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        MainWindow.setWindowIcon(icon)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.leFR = QtWidgets.QLineEdit(self.centralwidget)
        self.leFR.setGeometry(QtCore.QRect(20, 80, 321, 23))
        self.leFR.setObjectName("leFR")
        self.btnLoadFR = QtWidgets.QPushButton(self.centralwidget)
        self.btnLoadFR.setGeometry(QtCore.QRect(360, 80, 151, 21))
        self.btnLoadFR.setObjectName("btnLoadFR")
        self.btnLoadGabarito = QtWidgets.QPushButton(self.centralwidget)
        self.btnLoadGabarito.setGeometry(QtCore.QRect(360, 20, 151, 21))
        self.btnLoadGabarito.setObjectName("btnLoadGabarito")
        self.leGabarito = QtWidgets.QLineEdit(self.centralwidget)
        self.leGabarito.setGeometry(QtCore.QRect(20, 20, 321, 23))
        self.leGabarito.setObjectName("leGabarito")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(370, 110, 141, 21))
        self.label.setObjectName("label")
        
        self.grid_layout = QGridLayout(self.centralwidget)  # Create a QGridLayout
        self.centralwidget.setLayout(self.grid_layout)  # Install this placement in the central widget

        self.tableNotas = QtWidgets.QTableWidget(self.centralwidget)
        self.tableNotas.setGeometry(QtCore.QRect(20, 130, 291, 221))
        self.tableNotas.setObjectName("tableNotas")
        self.tableNotas.setColumnCount(2)
        self.tableNotas.setHorizontalHeaderLabels(["Nome", "Nota"])        
        
        self.lePeso = QtWidgets.QLineEdit(self.centralwidget)
        self.lePeso.setGeometry(QtCore.QRect(20, 50, 321, 23))
        self.lePeso.setText("")
        self.lePeso.setObjectName("lePeso")
        self.btnLoadPeso = QtWidgets.QPushButton(self.centralwidget)
        self.btnLoadPeso.setGeometry(QtCore.QRect(360, 50, 151, 21))
        self.btnLoadPeso.setObjectName("btnLoadPeso")
        self.btnStart = QtWidgets.QPushButton(self.centralwidget)
        self.btnStart.setGeometry(QtCore.QRect(360, 330, 151, 21))
        self.btnStart.setObjectName("btnStart")
        
        self.cbProva = QtWidgets.QComboBox(self.centralwidget)
        self.cbProva.setGeometry(QtCore.QRect(360, 160, 151, 21))
        self.cbProva.setObjectName("cbProva")
        self.cbProva.setMaxVisibleItems(4)
        self.cbProva.setMaxCount(4)
        self.cbProva.addItem('PDI')
        self.cbProva.addItem('REDES')
        self.cbProva.addItem('PDS')
        self.cbProva.addItem('CONTROLE')
        
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 534, 23))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        self.cbProva.setCurrentIndex(0)
        
        QtCore.QMetaObject.connectSlotsByName(MainWindow)
        MainWindow.setTabOrder(self.btnLoadGabarito, self.btnLoadPeso)
        MainWindow.setTabOrder(self.btnLoadPeso, self.btnLoadFR)
        MainWindow.setTabOrder(self.btnLoadFR, self.btnStart)
        MainWindow.setTabOrder(self.btnStart, self.leGabarito)
        MainWindow.setTabOrder(self.leGabarito, self.lePeso)
        MainWindow.setTabOrder(self.lePeso, self.leFR)
        MainWindow.setTabOrder(self.leFR, self.tableNotas)

        self.setConnects(MainWindow)
        self.clean()

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Super OMR 2000 Deluxe"))
        self.btnLoadFR.setText(_translate("MainWindow", "Carregar FR"))
        self.btnLoadGabarito.setText(_translate("MainWindow", "Carregar Gabarito"))
        self.label.setText(_translate("MainWindow", "FR: Folhas de Respostas."))
        self.btnLoadPeso.setText(_translate("MainWindow", "Carregar Peso"))
        self.btnStart.setText(_translate("MainWindow", "Mostrar Resultados"))

    def setConnects(self, MainWindow):
        self.btnLoadGabarito.clicked.connect(self.loadGabarito)
        self.btnLoadPeso.clicked.connect(self.loadPeso)
        self.btnLoadFR.clicked.connect(self.loadFR)
        self.cbProva.currentIndexChanged.connect(self.setProva)
        self.btnStart.clicked.connect(self.showResults)

    def clean(self):
        dir = 'files/gabarito/'
        for f in listdir(dir):
            remove(join(dir, f))
        dir = 'files/provas/'
        for f in listdir(dir):
            remove(join(dir, f))
        dir = 'files/peso/'
        for f in listdir(dir):
            remove(join(dir, f))
        if exists("files/answers.csv"):
            remove("files/answers.csv")
        else:
            print("Pronto para iniciar")

    def setProva(self, index):
        global prova 
        prova = index + 1
        print("Prova", prova, "Materia: ", self.cbProva.itemText(index))
        self.leGabarito.setText("")
        self.lePeso.setText("")
        self.leFR.setText("")
        self.tableNotas.clearContents()

    def loadGabarito(self):
        self.clean()
        path = QFileDialog.getOpenFileName(
            parent=self,
            caption="Selecione a imagem do gabarito",
            directory=".", 
            filter='Image file (*.png *.jpg *.jpeg)',
            initialFilter='Image file (*.png *.jpg *.jpeg)',
            options=QFileDialog.DontUseNativeDialog)
        filename = path[0].split("/")[-1]
        self.leGabarito.setText(filename)
        print(filename)
        original = cv2.imread(path[0])
        # output = warp(original)
        ret = createGabarito(original, self.cbProva.currentIndex())
        if(ret):
            self.statusbar.showMessage("Gabarito carregado com sucesso!")
        else:
            self.statusbar.showMessage("Problemas com gabarito")        
    
    def loadPeso(self):
        path = QFileDialog.getOpenFileName(
            parent=self,
            caption="Selecione o csv com os pesos",
            directory=".", 
            filter='Data file (*.csv)',
            initialFilter='Data file (*.csv)',
            options=QFileDialog.DontUseNativeDialog)
        filename = path[0].split("/")[-1]
        self.lePeso.setText(filename)
        print(filename)
        ret = createPeso(path, self.cbProva.currentIndex())
        if(ret):
            self.statusbar.showMessage("Peso carregado com sucesso!")
        else:
            self.statusbar.showMessage("Problemas com arquivo de peso") 

    def loadFR(self):
        path = QFileDialog.getExistingDirectory(
            parent=self,
            caption="Selecione a pasta com as Folhas de resposta",
            directory=".",
            options=QFileDialog.DontUseNativeDialog)
        pathName = path.split("/")[-1]
        self.leFR.setText(pathName)
        print(pathName)
        ret = findAnswers(path)
        if(ret):
            self.statusbar.showMessage("Folhas de prova carregadas com sucesso!")
        else:
            self.statusbar.showMessage("Problemas com folhas de prova") 

    def showResults(self):
        print("Prova", prova, "Materia: ", self.cbProva.currentText())
        if(showScore(self.cbProva.currentIndex())):
            self.statusbar.showMessage("Provas corrigidas com sucesso!")
        else:
            self.statusbar.showMessage("Problemas na correção das provas")
        
        i = 0
        filename = "files/answers.csv"
        with open(filename) as ans:
            r = csv.reader(ans)
            nRows = sum(1 for row in r)
            self.tableNotas.setRowCount(nRows)
            ans.seek(0)
            for rows in r:
                self.tableNotas.setItem(i,0, QTableWidgetItem(rows[0]))
                self.tableNotas.setItem(i,1, QTableWidgetItem(rows[1]))
                i += 1
        self.tableNotas.resizeColumnsToContents()
        self.tableNotas.horizontalHeader().setStretchLastSection(True)
        

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show() #FullScreen()
    sys.exit(app.exec_())
